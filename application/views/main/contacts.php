<section class="row tm-margin-b-l">
    <div class="col-12">
        <h2 class="tm-blue-text tm-margin-b-p">Напишите нам</h2>
    </div>
    <div class="col-md-6 col-sm-12 mb-md-0 mb-5 tm-overflow-auto">         
        <div class="mr-lg-5">
            <!-- contact form -->
            <form action="#" method="post" class="tm-contact-form">
                <div class="form-group">
                    <input type="text" id="contact_name" name="contact_name" class="form-control" placeholder="Имя"  required/>
                </div>
                <div class="form-group">                                                        
                    <input type="email" id="contact_email" name="contact_email" class="form-control" placeholder="Электронная почта"  required/>
                </div>
                <div class="form-group">
                    <textarea id="contact_message" name="contact_message" class="form-control" rows="8" placeholder="Сообщение" required></textarea>
                </div>
                <button type="submit" class="tm-btn tm-btn-blue float-right">Отправить</button>
            </form>                          
        </div>                                       
    </div>
    <div class="col-md-6 col-sm-12">
        <p class="tm-margin-b-p">Вы можете связаться с нами по любым интересующим вас вопросам по номеру телефона или электронной почте.</p>
        <address>
            <span class="tm-blue-text">Адрес</span><br>
            г. Бишкек<br>
            мкр-н Тунгуч 6-73<br><br>
            <div class="tm-blue-text">          
                Телефон: <a class="tm-blue-text" href="tel:+996702411838"> +996702411838</a><br>
                Whatsapp: <a class="tm-blue-text" href="https://api.whatsapp.com/send?phone=996702411838"> +996702411838</a><br>
                Instagram: <a class="tm-blue-text" href="https://www.instagram.com/kitep_kana"> kitep_kana</a><br> 
                Почта: <a class="tm-blue-text" href="mailto:info@company.com"> kitep-kana@mail.ru</a><br> 
            </div>                            
        </address>
    </div>
</section>