<section class="row tm-margin-b-l">
    <div class="col-12">
        <h2 class="tm-blue-text tm-margin-b-p">404 Ошибка</h2>
    </div>
    <p>Запрашиваемая страница не найдена, пожалуйста проверьте ссылку.</p>
</section>