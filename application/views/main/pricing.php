<section class="row tm-item-preview tm-margin-b-p tm-flex-jc-se">
  <div class="col-12">
    <h2 class="tm-blue-text tm-margin-b-p">Тарифы</h2>
  </div>
  <?php foreach ($subscriptions as $subscription): ?>
    <div class="col-md-5 col-sm-12 tm-highlight tm-small-pad tm-margin-b-p">
      <h2 class="tm-margin-b-p"><?= $subscription['title'] ?></h2>
      <p class="tm-margin-b-p">Данный тариф включает в себя перечень следующих доступных материалов:</p>
      <pre><p><?= $subscription['description'] ?></p></pre>
      <div class="tm-subscription-pricing">
        <span class="tm-subscription-pricing-time">1 месяц</span>
        <span class="tm-subscription-pricing-price"><?= $subscription['price'] ?> сом</span>
      </div>
    </div>
  <?php endforeach ?>
</section>