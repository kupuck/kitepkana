<section class="tm-margin-b-l">
    <header>
        <h2 class="tm-main-title">Добро пожаловать в наш магазин</h2>    
    </header>

    <p>Можно добавить какой либо текст.</p>

    <div class="tm-gallery">
        <div class="row">
            <?php if (isset($books)): ?>
            <?php foreach ($books as $book): ?>
                <figure class="col-lg-3 col-md-4 col-sm-6 col-12 tm-gallery-item">
                    <a href="/book/<?= $book['id'] ?>">
                        <div class="tm-gallery-item-overlay">
                            <img src="<?= $book['image'] ?>" alt="Image" class="img-fluid tm-img-center">
                        </div>
                        
                        <p class="tm-figcaption"><?= $book['title'] ?></p>
                    </a>
                </figure>
            <?php endforeach ?>
            <?php endif ?>
        </div>   
    </div>
</section>