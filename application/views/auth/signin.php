<section class="row tm-margin-b-l">
  <div class="col-12">
    <h2 class="tm-blue-text tm-margin-b-p">Выполните вход на сайт</h2>
  </div>
  <div class="col-md-6 col-sm-12 mb-md-0 mb-5 tm-overflow-auto">     
    <div class="mr-lg-5">
      <?php if (isset($errors)): ?>
        <div class="alert alert-danger alert-dismissible" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <?php foreach ($errors as $error): ?>
            <div> - <?= $error ?></div>
          <?php endforeach ?>
        </div>
      <?php endif ?>
      <form method="post" class="tm-contact-form">
        <div class="form-group">
          <label for="username">Логин</label>
          <input type="text" class="form-control" id="username" name="username" placeholder="Логин" required>
        </div>
        <div class="form-group">
          <label for="password">Пароль</label>
          <input type="password" class="form-control" id="password" name="password" placeholder="Пароль" required>
        </div>
        <button type="submit" class="tm-btn tm-btn-blue float-right">Вход</button>
      </form>                          
    </div>                                       
  </div>
</section>