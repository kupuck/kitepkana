<section class="row tm-margin-b-l">
    <div class="col-12">
        <h2 class="tm-blue-text tm-margin-b-p">Регистрация</h2>
    </div>
    <div class="col-md-6 col-sm-12 mb-md-0 mb-5 tm-overflow-auto">     
      <div class="mr-lg-5">
        <?php if (isset($errors)): ?>
          <div class="alert alert-danger alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <?php foreach ($errors as $error): ?>
              <div> - <?= $error ?></div>
            <?php endforeach ?>
          </div>
        <?php endif ?>
        <form method="post" enctype="multipart/form-data">
          <div class="form-group">
            <label for="first_name">Имя</label>
            <input type="text" class="form-control" id="first_name" name="first_name" placeholder="Имя" required>
          </div>
          <div class="form-group">
            <label for="last_name">Фамилия</label>
            <input type="text" class="form-control" id="last_name" name="last_name" placeholder="Фамилия" required>
          </div>
          <div class="form-group">
            <label for="email">Электронная почта</label>
            <input type="email" class="form-control" id="email" name="email" placeholder="Электронная почта" required>
          </div>
          <div class="form-group">
            <label for="username">Логин</label>
            <input type="text" class="form-control" id="username" name="username" pattern="^[a-zA-Z0-9]+$" placeholder="Лат. буквы или цифры" required>
          </div>
          <div class="form-group">
            <label for="password">Пароль</label>
            <input type="password" class="form-control" id="password" name="password" pattern="^[a-zA-Z0-9]+$" placeholder="Лат. буквы или цифры" required>
          </div>
          <div class="form-group">
            <label for="birthdate">Дата рождения</label>
            <input type="date" class="form-control" id="birthdate" name="birthdate" placeholder="Дата рождения" required>
          </div>
          <?php if (isset($subscriptions)): ?>
            <div class="form-group tm-signup-subscriptions tm-flex-jc-se">
              <label>Тариф</label>
              <?php foreach ($subscriptions as $subscription): ?>
                <div class="tm-signup-subscription">
                  <label for="subscription-<?= $subscription['id'] ?>"><?= $subscription['title'] ?></label>
                  <input type="radio" id="subscription-<?= $subscription['id'] ?>" name="subscription" value="<?= $subscription['id'] ?>" required>
                  <div class="tm-signup-subscription-description"><pre><p><?= $subscription['description'] ?></p></pre></div>
                  <div class="tm-subscription-pricing">
                    <span class="tm-subscription-pricing-time">1 месяц</span>
                    <span class="tm-subscription-pricing-price"><?= $subscription['price'] ?> сом</span>
                  </div>
                </div>
              <?php endforeach ?>
            </div>
          <?php endif ?>
          <button type="submit" class="btn btn-primary">Сохранить</button>
        </form>
      </div>
    </div>
</section>

<script>
  $(document).on('change', '.tm-signup-subscription input', event => {
    $('.tm-signup-subscription').removeClass('active');
    $(event.target).closest('.tm-signup-subscription').addClass('active');
  });
</script>