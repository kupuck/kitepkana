<section class="row tm-margin-b-l">
    <div class="col-12">
        <h2 class="tm-blue-text tm-margin-b-p">Админ панель - Тарифы</h2>
    </div>
    <div class="col-12">
      <table class="table table-responsive">
        <thead class="thead-dark">
          <tr>
            <th scope="col">#</th>
            <th scope="col">Наименование</th>
            <th scope="col">Цена</th>
            <th scope="col">Редактировать</th>
          </tr>
        </thead>
        <tbody>
          <?php foreach ($subscriptions as $subscription): ?>
            <tr>
              <th><?= $subscription['id'] ?></th>
              <td><?= $subscription['title'] ?></td>
              <td><?= $subscription['price'] ?></td>
              <td><a href="/admin/editsubscription/<?= $subscription['id'] ?>"><i class="fa fa-pencil" aria-hidden="true"></i></a></td>
            </tr>
          <?php endforeach ?>
        </tbody>
      </table>
    </div>
</section>