<section class="row tm-margin-b-l">
  <div class="col-12">
    <h2 class="tm-blue-text tm-margin-b-p">Админ панель - Подкатегории</h2>
  </div>
  <div class="col-3">
    <a href="/admin/addsubcollection" class="btn btn-success">Добавить</a>
  </div>
  <div class="col-12 margin-top-md">
    <table class="table table-responsive">
      <thead class="thead-dark">
        <tr>
          <th scope="col">#</th>
          <th scope="col">Название</th>
          <th scope="col">Редактировать</th>
          <th scope="col">Удалить</th>
        </tr>
      </thead>
      <tbody>
        <?php foreach ($subcollections as $subcollection): ?>
          <tr>
            <th><?= $subcollection['id'] ?></th>
            <td><?= $subcollection['title'] ?></td>
            <td><a href="/admin/editsubcollection/<?= $subcollection['id'] ?>"><i class="fa fa-pencil" aria-hidden="true"></i></a></td>
            <td><a href="/admin/removesubcollection/<?= $subcollection['id'] ?>"><i class="fa fa-trash-o" aria-hidden="true"></i></a></td>
          </tr>
        <?php endforeach ?>
      </tbody>
    </table>
  </div>
</section>