<section class="row tm-margin-b-l">
    <div class="col-12">
        <h2 class="tm-blue-text tm-margin-b-p">Админ панель</h2>
    </div>
    <div class="col-12">
      <ul class="list-group">
        <li class="list-group-item"><a href="/admin/orders">Заказы</a></li>
        <li class="list-group-item"><a href="/admin/buyorders">Заказы на покупку</a></li>
        <li class="list-group-item"><a href="/admin/books">Книги</a></li>
        <li class="list-group-item"><a href="/admin/collections">Категории</a></li>
        <li class="list-group-item"><a href="/admin/subcollections">Подкатегории</a></li>
        <li class="list-group-item"><a href="/admin/subscriptions">Тарифы</a></li>
      </ul>
    </div>
</section>