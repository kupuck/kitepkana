<section class="row tm-margin-b-l">
    <div class="col-12">
        <h2 class="tm-blue-text tm-margin-b-p">Админ панель - Заказы на покупку</h2>
    </div>
    <div class="col-12">
      <table class="table table-responsive">
        <thead class="thead-dark">
          <tr>
            <th scope="col">#</th>
            <th scope="col">Книга</th>
            <th scope="col">Варианты</th>
            <th scope="col">Адрес</th>
            <th scope="col">Телефон</th>
            <th scope="col">Дата заказа</th>
            <th scope="col">Дата возврата</th>
            <th scope="col">Подтвердить заказ</th>
            <th scope="col">Подтвердить возврат</th>
          </tr>
        </thead>
        <tbody>
          <?php foreach ($orders as $order): ?>
            <tr>
              <th><?= $order['id'] ?></th>
              <td><?= $order['title'] ?></td>
              <td><?= $order['variants'] ?></td>
              <td><?= $order['address'] ?></td>
              <td><?= $order['phone'] ?></td>
              <td><?= date('Y-m-d', $order['date_start']) ?></td>
              <td><?= $order['date_end'] ? date('Y-m-d', $order['date_end']) : '' ?></td>
              <td>
                <?php if ($order['approved']): ?>
                  <i class="fa fa-check-circle-o" aria-hidden="true"></i>
                <?php else: ?>
                  <a href="javascript:void(0)" class="js-approve-order" data-id="<?= $order['id'] ?>" data-book="<?= $order['book'] ?>">
                  <i class="fa fa-check" aria-hidden="true"></i>
                </a>
                <?php endif ?>
              </td>
              <td>
                <?php if ($order['returned']): ?>
                  <i class="fa fa-check-circle-o" aria-hidden="true"></i>
                <?php else: ?>
                  <a href="javascript:void(0)" class="js-approve-return" data-id="<?= $order['id'] ?>" data-book="<?= $order['book'] ?>">
                  <i class="fa fa-check" aria-hidden="true"></i>
                </a>
                <?php endif ?>
              </td>
            </tr>
          <?php endforeach ?>
        </tbody>
      </table>
    </div>
</section>

<script>
  $(document).ready(() => {

    $(document).on('click', '.js-approve-order', (event) => {
      let orderId = $(event.target).closest('a').data('id');
      let bookId = $(event.target).closest('a').data('book');
      $.post('/admin/approveorder', { orderId, bookId }, (response) => {
        if (response.status) {
          $(event.target).closest('a').replaceWith('<i class="fa fa-check-circle-o" aria-hidden="true"></i>');
        } else {
          $('.table').prepend(`<div class="alert alert-danger" role="alert">${response.message}</div>`);
        }
      }, 'json');
    });

    $(document).on('click', '.js-approve-return', (event) => {
      let orderId = $(event.target).closest('a').data('id');
      let bookId = $(event.target).closest('a').data('book');
      $.post('/admin/approvereturn', { orderId, bookId }, (response) => {
        if (response.status) {
          $(event.target).closest('a').replaceWith('<i class="fa fa-check-circle-o" aria-hidden="true"></i>');
        } else {
          $('.table').prepend(`<div class="alert alert-danger" role="alert">${response.message}</div>`);
        }
      }, 'json');
    });
  });
</script>