<section class="row tm-margin-b-l">
  <div class="col-12">
    <h2 class="tm-blue-text tm-margin-b-p">Админ панель - Редактирование подкатегории</h2>
  </div>
  <div class="col-6 col-sm-12">
    <form method="post" enctype="multipart/form-data">
      <div class="form-group">
        <label for="collection">Категория</label>
        <select class="form-control" id="collection" name="collection" required>
          <?php foreach ($collections as $collection): ?>
            <option value="<?= $collection['id'] ?>"><?= $collection['title'] ?></option>
          <?php endforeach ?>
        </select>
      </div>
      <div class="form-group">
        <label for="title">Название</label>
        <input type="text" class="form-control" id="title" name="title" placeholder="Название" value="<?= $subcollection['title'] ?>" required>
      </div>
      <button type="submit" class="btn btn-primary">Сохранить</button>
    </form>
  </div>
</section>