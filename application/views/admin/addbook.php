<section class="row tm-margin-b-l">
  <div class="col-12">
    <h2 class="tm-blue-text tm-margin-b-p">Админ панель - Добавление книги</h2>
  </div>
  <div class="col-6 col-sm-12">
    <form method="post" enctype="multipart/form-data">
      <div class="form-group">
        <label for="collection">Категория</label>
        <select class="form-control" id="subcollection" name="subcollection" required>
          <option value="0" selected="selected">Без категории</option>
          <?php foreach ($subcollections as $subcollection): ?>
            <option value="<?= $subcollection['id'] ?>"><?= $subcollection['title'] ?></option>
          <?php endforeach ?>
        </select>
      </div>
      <div class="form-group">
        <label for="author">Автор</label>
        <input type="text" class="form-control" id="author" name="author" placeholder="Автор" required>
      </div>
      <div class="form-group">
        <label for="title">Название</label>
        <input type="text" class="form-control" id="title" name="title" placeholder="Название" required>
      </div>
      <div class="form-group">
        <label for="description">Описание</label>
        <textarea name="description" id="description" class="form-control" cols="30" rows="10"></textarea>
      </div>
      <div class="form-group">
        <label for="price">Цена</label>
        <input type="number" class="form-control" id="price" name="price" placeholder="Цена" required>
      </div>
      <div class="form-group">
        <label for="quantity">Количество</label>
        <input type="number" class="form-control" id="quantity" name="quantity" placeholder="Количество" required>
      </div>
      <div class="form-group">
        <label for="image">Изображение</label>
        <input type="file" class="form-control" id="image" name="image" accept="image/*" required>
      </div>
      <button type="submit" class="btn btn-primary">Сохранить</button>
    </form>
  </div>
</section>

<script src="https://cdn.tiny.cloud/1/xrgah46442sqs8gkxckern04ikqa0u4ymm7dl1zkcvebchuh/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
<script>
  tinymce.init({
    selector: '#description'
  });
</script>