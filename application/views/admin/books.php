<section class="row tm-margin-b-l">
    <div class="col-12">
        <h2 class="tm-blue-text tm-margin-b-p">Админ панель - Книги</h2>
    </div>
    <div class="col-3">
      <a href="/admin/addbook" class="btn btn-success">Добавить</a>
    </div>
    <div class="col-12 margin-top-md">
      <table class="table table-responsive">
        <thead class="thead-dark">
          <tr>
            <th scope="col">#</th>
            <th scope="col">Автор</th>
            <th scope="col">Название</th>
            <th scope="col">Цена</th>
            <th scope="col">Количество</th>
            <th scope="col">Редактировать</th>
            <th scope="col">Удалить</th>
          </tr>
        </thead>
        <tbody>
          <?php foreach ($books as $book): ?>
            <tr>
              <th><?= $book['id'] ?></th>
              <td><?= $book['author'] ?></td>
              <td><?= $book['title'] ?></td>
              <td><?= $book['price'] ?></td>
              <td><?= $book['quantity'] ?></td>
              <td><a href="/admin/editbook/<?= $book['id'] ?>"><i class="fa fa-pencil" aria-hidden="true"></i></a></td>
              <td><a href="/admin/removebook/<?= $book['id'] ?>"><i class="fa fa-trash-o" aria-hidden="true"></i></a></td>
            </tr>
          <?php endforeach ?>
        </tbody>
      </table>
    </div>
</section>