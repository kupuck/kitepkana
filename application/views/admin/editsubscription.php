<section class="row tm-margin-b-l">
    <div class="col-12">
        <h2 class="tm-blue-text tm-margin-b-p">Админ панель - Редактирование тарифа</h2>
    </div>
    <div class="col-6 col-sm-12">
      <form method="post" enctype="multipart/form-data">
        <div class="form-group">
          <label for="title">Наименование</label>
          <input type="text" class="form-control" id="title" name="title" placeholder="Наименование" value="<?= $subscription['title'] ?>" required>
        </div>
        <div class="form-group">
          <label for="description">Описание</label>
          <textarea name="description" id="description" class="form-control" cols="30" rows="10"><?= $subscription['description'] ?></textarea>
        </div>
        <div class="form-group">
          <label for="price">Цена</label>
          <input type="number" class="form-control" id="price" name="price" placeholder="Цена" value="<?= $subscription['price'] ?>" required>
        </div>
        <button type="submit" class="btn btn-primary">Сохранить</button>
      </form>
    </div>
</section>