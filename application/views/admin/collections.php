<section class="row tm-margin-b-l">
  <div class="col-12">
    <h2 class="tm-blue-text tm-margin-b-p">Админ панель - Категории</h2>
  </div>
  <div class="col-3">
    <a href="/admin/addcollection" class="btn btn-success">Добавить</a>
  </div>
  <div class="col-12 margin-top-md">
    <table class="table table-responsive">
      <thead class="thead-dark">
        <tr>
          <th scope="col">#</th>
          <th scope="col">Название</th>
          <th scope="col">Редактировать</th>
          <th scope="col">Удалить</th>
        </tr>
      </thead>
      <tbody>
        <?php foreach ($collections as $collection): ?>
          <tr>
            <th><?= $collection['id'] ?></th>
            <td><?= $collection['title'] ?></td>
            <td><a href="/admin/editcollection/<?= $collection['id'] ?>"><i class="fa fa-pencil" aria-hidden="true"></i></a></td>
            <td><a href="/admin/removecollection/<?= $collection['id'] ?>"><i class="fa fa-trash-o" aria-hidden="true"></i></a></td>
          </tr>
        <?php endforeach ?>
      </tbody>
    </table>
  </div>
</section>