<section class="tm-margin-b-l">
  <header>
    <h2 class="tm-blue-text tm-margin-b-p">Категории</h2> 
  </header>
    <div class="col-12 margin-top-md">
      <table class="table table-responsive">
        <thead class="thead-dark">
          <tr>
            <th scope="col">Название</th>
            <th scope="col">Подкатегории</th>
          </tr>
        </thead>
        <tbody>
          <?php foreach ($collections as $collection): ?>
            <tr>
              <td><a href="/<?= !$collection['id'] ? 'sub' : '' ?>collection/<?= $collection['id'] ?>"><?= $collection['title'] ?></a></td>
              <td>
                <?php if (array_key_exists('subcollections', $collection)): ?>
                  <ul>
                    <?php foreach ($collection['subcollections'] as $subcollection): ?>
                      <li><a href="/subcollection/<?= $subcollection['id'] ?>"><?= $subcollection['title'] ?></a></li>
                    <?php endforeach ?>
                  </ul>
                <?php endif ?>
              </td>
            </tr>
          <?php endforeach ?>
        </tbody>
      </table>
    </div>
</section>