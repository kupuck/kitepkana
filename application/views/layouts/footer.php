        </div>
      </div>
      <footer>
        © <span class="tm-current-year"></span> Kitepkana
      </footer>
    <script src="/assets/js/jquery-1.11.3.min.js"></script>         <!-- jQuery (https://jquery.com/download/) -->
    <script src="/assets/js/popper.min.js"></script>                <!-- Popper (https://popper.js.org/) -->
    <script src="/assets/js/bootstrap.min.js"></script>      
    <script>
      $(document).ready(function() {
        $('.tm-current-year').text(new Date().getFullYear());
      });
   </script>
  </body>
</html>