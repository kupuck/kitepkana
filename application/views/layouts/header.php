<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="ru"> <!--<![endif]-->
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Китепкана <?= isset($title) && $title ? "- {$title}" : '' ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400">
    <link rel="stylesheet" href="/assets/fonts/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="/assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="/assets/css/styles.css">
    <link rel="stylesheet" href="/assets/css/toast.css">
    <link rel="shortcut icon" type="image/x-icon" href="/assets/img/icon.ico">
    <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
    <script src="/assets/js/toast.js"></script>
  </head>
  <body>
    <div class="container">
      <header class="tm-site-header">
        <!-- <h1 class="tm-site-name">Shelf</h1>
        <p class="tm-site-description">Your Online Bookstore</p> -->
        <nav class="navbar navbar-expand-md tm-main-nav-container">
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#tmMainNav" aria-controls="tmMainNav" aria-expanded="false" aria-label="Toggle navigation">
            <i class="fa fa-bars"></i>
          </button>
          <div class="collapse navbar-collapse tm-main-nav" id="tmMainNav">
            <ul class="nav nav-fill tm-main-nav-ul">
              <li class="nav-item"><a class="nav-link <?= isset($title) && $title === 'Главная' ? 'active' : '' ?>" href="/">Главная</a></li>
              <li class="nav-item"><a class="nav-link <?= isset($title) && $title === 'Категории' ? 'active' : '' ?>" href="/collections">Категории</a></li>
              <li class="nav-item"><a class="nav-link <?= isset($title) && $title === 'Новинки' ? 'active' : '' ?>" href="/new">Новинки</a></li>
              <li class="nav-item"><a class="nav-link <?= isset($title) && $title === 'Поиск' ? 'active' : '' ?>" href="/search">Поиск</a></li>
              <li class="nav-item"><a class="nav-link <?= isset($title) && $title === 'Тарифы' ? 'active' : '' ?>" href="/pricing">Тарифы</a></li>
              <li class="nav-item"><a class="nav-link <?= isset($title) && $title === 'Контакты' ? 'active' : '' ?>" href="/contacts">Контакты</a></li>
              <?php if (isset($isSignedIn) && $isSignedIn): ?>
                <?php if (isset($isAdmin) && $isAdmin): ?>
                  <li class="nav-item"><a class="nav-link <?= isset($title) && $title === 'Выход' ? 'active' : '' ?>" href="/admin">Админ панель</a></li>
                <?php endif ?>
                <li class="nav-item"><a class="nav-link <?= isset($title) && $title === 'Выход' ? 'active' : '' ?>" href="/signout">Выход</a></li>
              <?php else: ?>
                <li class="nav-item"><a class="nav-link <?= isset($title) && $title === 'Вход' ? 'active' : '' ?>" href="/signin">Вход</a></li>
                <li class="nav-item"><a class="nav-link <?= isset($title) && $title === 'Регистрация' ? 'active' : '' ?>" href="/signup">Регистрация</a></li>
              <?php endif ?>
            </ul>
          </div>
        </nav>
      </header>
      <div class="tm-main-content">