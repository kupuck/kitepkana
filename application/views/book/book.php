<section class="row tm-item-preview">
  <div class="col-md-6 col-sm-12 mb-md-0 mb-5">
    <img src="<?= $book['image'] ?>" alt="website template image" class="img-fluid tm-img-center-sm">
  </div>
  <div class="col-md-6 col-sm-12">
    <h2 class="tm-blue-text tm-margin-b-p"><?= $book['title'] ?></h2>
    <?= $book['description'] ?>
    <br />
    <p class="tm-blue-text tm-margin-b-s">
      <strong>Цена: </strong> <?= $book['price'] ?> сом
    </p>
    <p class="tm-blue-text tm-margin-b-s">
      <strong>Количество: </strong> <?= $book['quantity'] ?> шт
    </p>
    <a href="javascript:void(0)" class="tm-btn tm-btn-gray tm-margin-r-20 tm-margin-b-s js-buy-modal">Купить</a>
    <?php if ($isSignedIn): ?>
      <a href="javascript:void(0)" class="tm-btn tm-btn-blue js-order-modal">Заказать</a>
    <?php endif ?>
  </div>
</section>

<div id="order-book-modal" class="modal fade" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Детали заказа</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
      <div class="modal-body">
        <div class="form-group">
          <label for="order-ebook" class="control-label">Электронный вариант:</label>
          <input type="checkbox" id="order-ebook">
        </div>
        <div class="form-group">
          <label for="order-audio" class="control-label">Аудио вариант:</label>
          <input type="checkbox" id="order-audio">
        </div>
        <div class="form-group">
          <label for="order-paper" class="control-label">Бумажный вариант:</label>
          <input type="checkbox" id="order-paper">
        </div>
        <div class="form-group">
          <label for="order-address" class="control-label">Адрес:</label>
          <input type="text" class="form-control" id="order-address">
        </div>
        <div class="form-group">
          <label for="order-phone" class="control-label">Телефон:</label>
          <input type="text" class="form-control" id="order-phone">
        </div>
        <div class="form-group">
          <input type="hidden" class="form-control" id="order-book-id" value="<?= $book['id'] ?>">
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="tm-btn tm-btn-blue js-order">Заказать</button>
      </div>
    </div>
  </div>
</div>

<div id="buy-book-modal" class="modal fade" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Детали заказа</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
      <div class="modal-body">
        <div class="form-group">
          <label for="order-address" class="control-label">Адрес:</label>
          <input type="text" class="form-control" id="order-address">
        </div>
        <div class="form-group">
          <label for="order-phone" class="control-label">Телефон:</label>
          <input type="text" class="form-control" id="order-phone">
        </div>
        <div class="form-group">
          <input type="hidden" class="form-control" id="order-book-id" value="<?= $book['id'] ?>">
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="tm-btn tm-btn-blue js-buy">Купить</button>
      </div>
    </div>
  </div>
</div>

<script>
  function makeOrder(event) {
    $(event.target).attr('disabled', 'disabled');
    let modal = $(event.target).closest('.modal.fade');
    let data = {
      address: modal.find('#order-address').val(),
      phone: modal.find('#order-phone').val(),
      book: modal.find('#order-book-id').val()
    };
    data.audio = (modal.attr('id') === 'buy-book-modal') ? 0 : (modal.find('#order-audio').is(':checked') ? 1 : 0);
    data.ebook = (modal.attr('id') === 'buy-book-modal') ? 0 : (modal.find('#order-ebook').is(':checked') ? 1 : 0);
    data.paper = (modal.attr('id') === 'buy-book-modal') ? 1 : (modal.find('#order-paper').is(':checked') ? 1 : 0);
    data.type = (modal.attr('id') === 'buy-book-modal') ? 'buy' : 'order';
    $.post('/books/order', data, response => {
      if (response.status) {
        modal.modal('hide');
      } else {
        $(event.target).removeAttr('disabled');
      }
      $.toast(response.message);
    }, 'json');
  };

  $(document).on('click', '.js-order-modal', () => $('#order-book-modal').modal());
  $(document).on('click', '.js-buy-modal', () => $('#buy-book-modal').modal());
  $(document).on('click', '.js-order', event => makeOrder(event));
  $(document).on('click', '.js-buy', event => makeOrder(event));
</script>