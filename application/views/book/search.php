<section class="tm-margin-b-l">
  <header>
    <h2 class="tm-blue-text tm-margin-b-p"><?= $title ?></h2> 
  </header>
  <div class="col-md-6 col-lg-6 col-sm-12">
    <form method="post">
      <div class="input-group">
        <input class="form-control" name="query" placeholder="Название, Автор ..." value="<?= $query ?>">
        <span class="input-group-btn">
          <button class="tm-btn tm-btn-blue search-btn" type="submit"><i class="fa fa-search" aria-hidden="true"></i></button>
        </span>
      </div>
    </form>
  </div>
  <div class="tm-gallery">
    <div class="row">
        <?php if (count($books)): ?>
          <?php foreach ($books as $book): ?>
            <figure class="col-lg-3 col-md-4 col-sm-6 col-12 tm-gallery-item">
              <a href="/book/<?= $book['id'] ?>">
                <div class="tm-gallery-item-overlay">
                  <img src="<?= $book['image'] ?>" alt="Image" class="img-fluid tm-img-center">
                </div>
                <p class="tm-figcaption"><?= $book['title'] ?></p>
              </a>
            </figure>
          <?php endforeach ?>
        <?php elseif (!count($books) && $query): ?>
          <h4>К сожалению, по вашему запросу книг не найдено.</h4>
        <?php endif ?>
      </div>   
    </div>
</section>