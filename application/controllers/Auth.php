<?php

defined('BASEPATH') OR exit('No direct script access allowed');
require_once('Base.php');

class Auth extends Base
{
	public function signIn()
	{
		$data = [];
		if ($this->isSignedIn()) {
			header('Location: /');
		}
		if ($this->input->server('REQUEST_METHOD') === 'POST') {
			$username = $this->input->post('username');
			$password = $this->input->post('password');
			$user = $this->checkAuth($username, $password);
			if ($user) {
				$this->setUserSession($user['id'], $user['type']);
				header('Location: /');
			} else {
				$data['errors'] = ['Неверные логин или пароль'];
			}
		}
		$this->loadViewWithLayouts('Вход', 'auth/signin', $data);
	}

	public function signUp()
	{
		$data = [];
		if ($this->isSignedIn()) {
			header('Location: /');
		}
		if ($this->input->server('REQUEST_METHOD') === 'POST') {
			$postData = $this->input->post();
			$validation = $this->validateSignUpData($postData);
			if ($validation['status']) {
				$this->load->model('User_model', 'user');
				$postData['password'] = md5($postData['password']);
				$userId = $this->user->add($postData);
				$this->setUserSession($userId, 'user');
				header('Location: /');
			} else {
				$data['errors'] = $validation['messages'];
			}
		}
		$this->load->model('Subscription_model', 'subscription');
		$data['subscriptions'] = $this->subscription->getAll();
		$this->loadViewWithLayouts('Регистрация', 'auth/signup', $data);
	}

	public function signOut()
	{
		if (!$this->isSignedIn()) {
			header('Location: /');
		}
		$this->clearUserSession();
		header('Location: /');
	}

	private function validateSignUpData($data)
	{
		$result = ['status' => false, 'messages' => []];
		if ($data) {
			if (!array_key_exists('first_name', $data) || !$data['first_name']) {
				$result['messages'][] = 'Значение для поля "Имя пользователя" некорректное.';
			}
			if (!array_key_exists('last_name', $data) || !$data['last_name']) {
				$result['messages'][] = 'Значение для поля "Фамилия пользователя" некорректное.';
			}
			if (!array_key_exists('email', $data) || !$data['email'] || strpos($data['email'], '@') < 0) {
				$result['messages'][] = 'Значение для поля "Электронная почта" некорректное.';
			}
			if (!array_key_exists('username', $data) || !$data['username'] || !preg_match('/^[a-zA-Z0-9]+$/', $data['username'])) {
				$result['messages'][] = 'Значение для поля "Логин" некорректное.';
			}
			if (!array_key_exists('password', $data) || !$data['password'] || !preg_match('/^[a-zA-Z0-9]+$/', $data['password'])) {
				$result['messages'][] = 'Значение для поля "Пароль" некорректное.';
			}
			if (!array_key_exists('birthdate', $data) || !$data['birthdate'] || date('Y-m-d', strtotime($data['birthdate'])) !== $data['birthdate']) {
				$result['messages'][] = 'Значение для поля "Дата рождения" некорректное.';
			}
			if ($this->checkExistingUser($data['email'], $data['username'])) {
				$result['messages'][] = 'Данные электронная почта или логин уже заняты.';
			}
		} else {
			$result['messages'][] = 'Системная ошибка, попробуйте позже.';
		}
		if (!count($result['messages'])) {
			$result['status'] = true;
		}
		return $result;
	}

	private function checkExistingUser($email, $username)
	{
		if ($email && $username) {
			$this->load->model('User_model', 'user');
			$user = $this->user->checkExistingUser($email, $username);
			if (count($user)) {
				return true;
			}
		}
		return false;
	}
}