<?php

defined('BASEPATH') OR exit('No direct script access allowed');
require_once('Base.php');

class Books extends Base
{  
  public function book($id)
  {
    $this->load->model('Book_model', 'book');
    $book = $this->book->getById($id);
    if (count($book)) {
      $this->loadViewWithLayouts('Книга', 'book/book', ['book' => $book[0]]);
    } else {
      $this->loadViewWithLayouts('Книга', 'main/404');
    }
  }

  public function order()
  {
    $result = [
      'status' => false,
      'message' => 'Ошибка. Попробуйте еще раз немного позже.'
    ];
    if ($this->input->server('REQUEST_METHOD') === 'POST') {
      $orderType = $this->input->post('type');
      if ($orderType === 'buy' || ($orderType === 'order' && $this->isSignedIn())) {
        $data = [
          'address' => $this->input->post('address'),
          'phone' => $this->input->post('phone'),
          'ebook' => $this->input->post('ebook'),
          'audio' => $this->input->post('audio'),
          'paper' => $this->input->post('paper'),
          'bookId' => $this->input->post('book'),
          'userId' => $orderType === 'buy' ? 0 : $this->getUserId()
        ];
        if (!$data['ebook'] && !$data['audio'] && !$data['paper']) {
          $result['message'] = 'Выберите какой нибудь вариант книги.';
        } else {
          $this->load->model('Order_model', 'order');
          try {
            $this->order->makeOrder($data);
            $result['status'] = true;
            $result['message'] = 'Ваш заказ успешно сохранен.';
          } catch (Exception $e) {
            $result['message'] = 'Введенные данные некорректные, пожалуйста проверьте.';
          }
        }
      }
    }
    $this->returnJson($result);
  }

  public function search()
  {
    $books = [];
    $query = '';
    if ($this->input->server('REQUEST_METHOD') === 'POST') {
      $query = $this->input->post('query');
      if ($query) {
        $this->load->model('Book_model', 'book');
        $books = $this->book->search($query);
      }
    }
    $this->loadViewWithLayouts('Поиск', 'book/search', ['books' => $books, 'query' => $query]);
  }
}