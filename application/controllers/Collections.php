<?php

defined('BASEPATH') OR exit('No direct script access allowed');
require_once('Base.php');

class Collections extends Base
{
	public function index()
	{
		$this->load->model('Collection_model', 'collection');
		$collections = $this->collection->getAllWithSubcollections();
		$collections[] = [
			'id' => 0,
			'title' => 'Книги без категории',
			'subcollections' => []
		];
		$this->loadViewWithLayouts('Книга', 'collections/index', ['collections' => $collections]);
	}

	public function new()
	{
    $this->load->model('Book_model', 'book');
    $books = $this->book->getPaginatedData();
		$this->loadViewWithLayouts('Новые поступления', 'collections/collection', ['books' => $books]);
	}

	public function collection($id)
	{
    $this->load->model('Book_model', 'book');
    $this->load->model('Collection_model', 'collection');
		$books = $this->book->getByCollectionId($id);
		$collection = $this->collection->getById($id);
		$collection = count($collection) ? $collection[0] : [];
		$title = $collection ? $collection['title'] : 'Книги';
		$this->loadViewWithLayouts($title, 'collections/collection', [
			'books' => $books,
			'collection' => $collection
		]);
	}

	public function subcollection($id)
	{
    $this->load->model('Book_model', 'book');
    $this->load->model('Subcollection_model', 'subcollection');
		$books = $this->book->getBySubcollectionId($id);
		$subcollection = $this->subcollection->getById($id);
		$subcollection = count($subcollection) ? $subcollection[0] : [];
		$title = $subcollection ? $subcollection['title'] : 'Книги';
		$this->loadViewWithLayouts($title, 'collections/collection', [
			'books' => $books,
			'collection' => $subcollection
		]);
	}
}

?>