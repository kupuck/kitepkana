<?php

defined('BASEPATH') OR exit('No direct script access allowed');
require_once('Base.php');

class Admin extends Base
{
  public function index()
  {
    $this->loadViewWithLayouts('Админ панель', 'admin/index');
  }

  public function collections()
  {
    $this->load->model('Collection_model', 'collection');
    $collections = $this->collection->getAll();
    $this->loadViewWithLayouts('Админ панель', 'admin/collections', ['collections' => $collections]);
  }

  public function addCollection()
  {
    if ($this->input->server('REQUEST_METHOD') === 'POST') {
      $this->load->model('Collection_model', 'collection');
      $data = $this->input->post();
      $this->collection->add($data);
    }
    $this->loadViewWithLayouts('Админ панель', 'admin/addcollection');
  }

  public function editCollection($id)
  {
    if ($id && is_numeric($id)) {
      $this->load->model('Collection_model', 'collection');
      if ($this->input->server('REQUEST_METHOD') === 'POST') {
        $data = $this->input->post();
        $this->collection->updateById($id, $data);
        header('Location: /admin/collections');
      } else {
        $collection = $this->collection->getById($id);
        if (count($collection)) {
          $this->loadViewWithLayouts('Админ панель', 'admin/editcollection', ['collection' => $collection[0]]);
        } else {
          header('Location: /admin/collections');
        }
      }
    } else {
      header('Location: /admin/collections');
    }
  }

  public function removeCollection($id)
  {
    if ($id && is_numeric($id)) {
      $this->load->model('Collection_model', 'collection');
      $this->collection->removeById($id);
    }
    header('Location: /admin/collections');
  }

  public function subcollections()
  {
    $this->load->model('Subcollection_model', 'subcollection');
    $subcollections = $this->subcollection->getAll();
    $this->loadViewWithLayouts('Админ панель', 'admin/subcollections', ['subcollections' => $subcollections]);
  }

  public function addSubcollection()
  {
    $this->load->model('Collection_model', 'collection');
    $collections = $this->collection->getAll();
    if ($this->input->server('REQUEST_METHOD') === 'POST') {
      $this->load->model('Subcollection_model', 'subcollection');
      $data = $this->input->post();
      $this->subcollection->add($data);
    }
    $this->loadViewWithLayouts('Админ панель', 'admin/addsubcollection', ['collections' => $collections]);
  }

  public function editSubcollection($id)
  {
    if ($id && is_numeric($id)) {
      $this->load->model('Subcollection_model', 'subcollection');
      if ($this->input->server('REQUEST_METHOD') === 'POST') {
        $data = $this->input->post();
        $this->subcollection->updateById($id, $data);
        header('Location: /admin/subcollections');
      } else {
        $subcollection = $this->subcollection->getById($id);
        if (count($subcollection)) {
          $this->load->model('Collection_model', 'collection');
          $collections = $this->collection->getAll();
          $this->loadViewWithLayouts('Админ панель', 'admin/editsubcollection', [
            'subcollection' => $subcollection[0],
            'collections' => $collections
          ]);
        } else {
          header('Location: /admin/subcollections');
        }
      }
    } else {
      header('Location: /admin/subcollections');
    }
  }

  public function removeSubcollection($id)
  {
    if ($id && is_numeric($id)) {
      $this->load->model('Subcollection_model', 'subcollection');
      $this->subcollection->removeById($id);
    }
    header('Location: /admin/subcollections');
  }

	public function books()
	{
		$this->load->model('Book_model', 'book');
		$data = [
			'books' => $this->book->getPaginatedData()
		];
    $this->loadViewWithLayouts('Админ панель', 'admin/books', $data);
  }

  public function addBook()
  {
    $this->load->model('Subcollection_model', 'subcollection');
    $subcollections = $this->subcollection->getAll();
    if ($this->input->server('REQUEST_METHOD') === 'POST') {
      $this->initUpload();
      if ($this->upload->do_upload('image')) {
        $this->load->model('Book_model', 'book');
        $file = $this->upload->data();
        $data = $this->input->post();
        $data['image'] = "/assets/img/books/{$file['file_name']}";
        $this->book->add($data);
        $this->loadViewWithLayouts('Админ панель', 'admin/addbook', ['subcollections' => $subcollections]);
      } else {
        print_r($this->upload->display_errors());
      }
    } else {
      $this->loadViewWithLayouts('Админ панель', 'admin/addbook', ['subcollections' => $subcollections]);
    }
  }
  
  public function editBook($id)
  {
    if ($id && is_numeric($id)) {
      $this->load->model('Book_model', 'book');
      if ($this->input->server('REQUEST_METHOD') === 'POST') {
        $data = $this->input->post();
        if (!empty($_FILES['image']['name'])) {
          $this->initUpload();
          if ($this->upload->do_upload('image')) {
            $this->removeBookImage($id);
            $file = $this->upload->data();
            $data['image'] = "/assets/img/books/{$file['file_name']}";
          } else {
            print_r($this->upload->display_errors());
          }
        }
        $this->book->updateById($id, $data);
        header('Location: /admin/books');
      } else {
        $book = $this->book->getById($id);
        if (count($book)) {
          $this->load->model('Subcollection_model', 'subcollection');
          $subcollections = $this->subcollection->getAll();
          $data = [
            'book' => $book[0],
            'subcollections' => $subcollections
          ];
          $this->loadViewWithLayouts('Админ панель', 'admin/editbook', $data);
        } else {
          header('Location: /admin/books');
        }
      }
    } else {
      header('Location: /admin/books');
    }
  }

  public function removeBook($id)
  {
    if ($id && is_numeric($id)) {
      $this->removeBookImage($id);
      $this->load->model('Book_model', 'book');
      $this->book->removeById($id);
    }
    header('Location: /admin/books');
  }

  public function orders()
  {
    $this->load->model('Order_model', 'order');
    $orders = $this->order->getForAdmin();
    $this->loadViewWithLayouts('Админ панель', 'admin/orders', ['orders' => $orders]);
  }

  public function buyOrders()
  {
    $this->load->model('Order_model', 'order');
    $orders = $this->order->getBuyOrdersForAdmin();
    $this->loadViewWithLayouts('Админ панель', 'admin/buyorders', ['orders' => $orders]);
  }

  public function approveOrder()
  {
    $result = [
      'status' => 0,
      'message' => 'Ошибка. Попробуйте еще раз немного позже.'
    ];
    if ($this->input->server('REQUEST_METHOD') === 'POST') {
      $orderId = $this->input->post('orderId');
      $bookId = $this->input->post('bookId');
      if ($orderId && $bookId) {
        $this->load->model('Order_model', 'order');
        $this->order->approveOrder($orderId, $bookId);
        $result = [
          'status' => 1,
          'message' => 'Success'
        ];
      }
    }
    $this->returnJson($result);
  }

  public function approveReturn()
  {
    $result = [
      'status' => 0,
      'message' => 'Ошибка. Попробуйте еще раз немного позже.'
    ];
    if ($this->input->server('REQUEST_METHOD') === 'POST') {
      $orderId = $this->input->post('orderId');
      $bookId = $this->input->post('bookId');
      if ($orderId && $bookId) {
        $this->load->model('Order_model', 'order');
        $this->order->approveReturn($orderId, $bookId);
        $result = [
          'status' => 1,
          'message' => 'Success'
        ];
      }
    }
    $this->returnJson($result);
  }

  public function subscriptions()
  {
    $this->load->model('Subscription_model', 'subscription');
    $subscriptions = $this->subscription->getAll();
    $this->loadViewWithLayouts('Админ панель', 'admin/subscriptions', ['subscriptions' => $subscriptions]);
  }

  public function editSubscription($id)
  {
    if ($id && is_numeric($id)) {
      $this->load->model('Subscription_model', 'subscription');
      if ($this->input->server('REQUEST_METHOD') === 'POST') {
        $data = $this->input->post();
        $this->subscription->updateById($id, $data);
        header('Location: /admin/subscriptions');
      } else {
        $subscription = $this->subscription->getById($id);
        if (count($subscription)) {
          $this->loadViewWithLayouts('Админ панель', 'admin/editsubscription', ['subscription' => $subscription[0]]);
        } else {
          header('Location: /admin/subscriptions');
        }
      }
    } else {
      header('Location: /admin/subscriptions');
    }
  }

  private function initUpload()
  {
    $config['upload_path'] = './assets/img/books';
    $config['allowed_types'] = 'gif|jpg|png';
    $config['encrypt_name'] = TRUE;
    $config['max_size'] = 10000;
    $config['file_name'] = time();
    $this->load->library('upload', $config);
  }

  private function removeBookImage($id)
  {
    $this->load->model('Book_model', 'book');
    $book = $this->book->getById($id);
    if (count($book)) {
      $image = substr($book[0]['image'], 1);
      $projectPath = str_replace('\\', '/', FCPATH);
      $fullImagePath = $projectPath . $image;
      if (file_exists($fullImagePath)) {
        unlink($fullImagePath);
      }
    }
  }
}