<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Base extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->library('session');
		$this->security();
	}
	
	public function loadViewWithLayouts($title = '', $view, $data = [])
	{
		$this->load->view('layouts/header', [
			'title' => $title,
			'isSignedIn' => $this->isSignedIn(),
			'isAdmin' => $this->isAdmin()
		]);
		$this->load->view($view, $data);
		$this->load->view('layouts/footer');
	}

	public function returnJson($data = [])
	{
		$json = json_encode($data);
		return $this->output->set_content_type('application/json')
			->set_status_header(200)
			->set_output($json);
	}

	public function checkAuth($username, $password)
	{
		if ($username && $password) {
			$this->load->model('User_model', 'user');
			$password = md5($password);
			$user = $this->user->getByUsernameAndPassword($username, $password);
			if (count($user)) {
				return $user[0];
			}
		}
		return false;
	}

	public function setUserSession($userId, $userType)
	{
		$this->session->set_userdata([
			'userId' => $userId,
			'userType' => $userType
		]);
	}

	public function clearUserSession()
	{
		$this->session->unset_userdata('userId');
		$this->session->unset_userdata('userType');
	}

	public function isSignedIn()
	{
		if ($this->session->has_userdata('userId')) {
			return true;
		}
		return false;
	}

	public function isAdmin()
	{
		if ($this->session->has_userdata('userType')
				&& $this->session->userdata('userType') === 'admin') {
					return true;
		}
		return false;
	}

	public function getUserId()
	{
		if ($this->isSignedIn()) {
			return $this->session->userdata('userId');
		}
		return 0;
	}

	public function security()
	{
		$controller = $this->uri->segment(1);
		if (strpos($controller, 'admin') > -1 && !$this->isAdmin()) {
			header('Location: /');
		}
		return true;
	}
}
