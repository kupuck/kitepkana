<?php

defined('BASEPATH') OR exit('No direct script access allowed');
require_once('Base.php');

class Main extends Base
{
	public function index()
	{
		$this->load->model('Book_model', 'book');
		$data = [
			'books' => $this->book->getForMainPage()
		];
    $this->loadViewWithLayouts('Главная', 'main/index', $data);
	}

	public function contacts()
	{
    $this->loadViewWithLayouts('Контакты', 'main/contacts');
	}

	public function pricing()
	{
    $this->load->model('Subscription_model', 'subscription');
    $subscriptions = $this->subscription->getAll();
    $this->loadViewWithLayouts('Тарифы', 'main/pricing', ['subscriptions' => $subscriptions]);
	}
}
