<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once('Base.php');

class My404 extends Base
{
	public function index()
	{
    $this->loadViewWithLayouts('Ошибка', 'main/404');
	}
}
