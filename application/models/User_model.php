<?php

class User_model extends CI_Model {
  public function __construct()
  {
    parent::__construct();
    $this->load->database();
  }

  public function getByUsernameAndPassword($username, $password)
  {
    $query = $this->db->get_where('users', ['username' => $username, 'password' => $password]);
    return $query->result_array();
  }

  public function checkExistingUser($email, $username)
  {
    $this->db->where('email', $email);
    $this->db->or_where('username', $username);
    $query = $this->db->get('users');
    return $query->result_array();
  }

  public function add($data)
  {
    $this->db->set($data);
    $this->db->insert('users');
    return $this->db->insert_id();
  }
}

?>