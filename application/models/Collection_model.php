<?php

class Collection_model extends CI_Model {
  public function __construct()
  {
    parent::__construct();
    $this->load->database();
  }

  public function getAll()
  {
    $query = $this->db->get('collections');
    return $query->result_array();
  }

  public function getAllWithSubcollections()
  {
    $columns = [
      'collections.id',
      'collections.title',
      'subcollections.id AS subcollection_id',
      'subcollections.title AS subcollection_title',
    ];
    $this->db->select($columns);
    $this->db->from('collections');
    $this->db->join('subcollections', 'collections.id = subcollections.collection', 'left');
    $query = $this->db->get();
    $result = $query->result_array();
    $sortedResult = [];
    foreach ($result as $row) {
      $sortedResult[$row['id']]['id'] = $row['id'];
      $sortedResult[$row['id']]['title'] = $row['title'];
      if ($row['subcollection_id'] && $row['subcollection_title']) {
        $sortedResult[$row['id']]['subcollections'][] = [
          'id' => $row['subcollection_id'],
          'title' => $row['subcollection_title']
        ];
      }
    }
    return $sortedResult;
  }

  public function getById($id)
  {
    $query = $this->db->get_where('collections', ['id' => $id]);
    return $query->result_array();
  }

  public function add($data)
  {
    $this->db->set($data);
    $this->db->insert('collections');
  }

  public function updateById($id, $data)
  {
    $this->db->where('id', $id);
    $this->db->set($data);
    $this->db->update('collections');
  }

  public function removeById($id)
  {
    $this->db->where('id', $id);
    $this->db->delete('collections');
  }
}

?>