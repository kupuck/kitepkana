<?php

class Order_model extends CI_Model {
  public function __construct()
  {
    parent::__construct();
    $this->load->database();
  }

  public function getForAdmin()
  {
    $columns = [
      'orders.id',
      'orders.book',
      'orders.address',
      'orders.phone',
      'orders.variants',
      'orders.date_start',
      'orders.date_end',
      'orders.approved',
      'orders.returned',
      'users.first_name',
      'users.last_name',
      'books.title'
    ];
    $this->db->select($columns);
    $this->db->from('orders');
    $this->db->join('users', 'orders.user = users.id');
    $this->db->join('books', 'orders.book = books.id');
    $this->db->order_by('orders.id', 'DESC');
    $query = $this->db->get();
    return $query->result_array();
  }

  public function getBuyOrdersForAdmin()
  {
    $columns = [ 'orders.*', 'books.title' ];
    $this->db->select($columns);
    $this->db->from('orders');
    $this->db->join('books', 'orders.book = books.id');
    $this->db->where('orders.user', 0);
    $this->db->order_by('orders.id', 'DESC');
    $query = $this->db->get();
    return $query->result_array();
  }

  public function makeOrder($data)
  {
    $newData = [
      'user' => $data['userId'],
      'book' => $data['bookId'],
      'variants' => '',
      'address' => $data['address'],
      'phone' => $data['phone'],
      'date_start' => time(),
      'date_end' => ''
    ];
    if ($data['ebook']) {
      $newData['variants'] .= $newData['variants'] ? ', электронный' : 'электронный';
    }
    if ($data['audio']) {
      $newData['variants'] .= $newData['variants'] ? ', аудио' : 'аудио';
    }
    if ($data['paper']) {
      $newData['variants'] .= $newData['variants'] ? ', бумажный' : 'бумажный';
    }
    $this->db->set($newData);
    $this->db->insert('orders');
  }

  public function approveOrder($id, $bookId)
  {
    $this->db->where('id', $id);
    $this->db->set(['approved' => 1]);
    $this->db->update('orders');
    $this->db->where('id', $bookId);
    $this->db->set('quantity', 'quantity - 1', false);
    $this->db->update('books');
  }

  public function approveReturn($id, $bookId)
  {
    $this->db->where('id', $id);
    $this->db->set(['returned' => 1]);
    $this->db->update('orders');
    $this->db->where('id', $bookId);
    $this->db->set('quantity', 'quantity + 1', false);
    $this->db->update('books');
  }
}

?>