<?php

class Book_model extends CI_Model {
  public function __construct()
  {
    parent::__construct();
    $this->load->database();
  }

  public function getForMainPage()
  {
    $this->db->order_by('id', 'RANDOM');
    $query = $this->db->get('books', 8);
    return $query->result_array();
  }

  public function getPaginatedData($limit = 10, $page = 1, $order = 'DESC')
  {
    $offset = ($page - 1) * $limit;
    $this->db->order_by('id', $order);
    $query = $this->db->get('books', $limit, $offset);
    return $query->result_array();
  }

  public function getByCollectionId($id)
  {
    $this->db->select('books.*');
    $this->db->from('collections');
    $this->db->join('subcollections', 'subcollections.collection = collections.id');
    $this->db->join('books', 'books.subcollection = subcollections.id');
    $this->db->where('collections.id', $id);
    $this->db->order_by('books.id', 'DESC');
    $query = $this->db->get();
    return $query->result_array();
  }

  public function getBySubcollectionId($id)
  {
    $query = $this->db->get_where('books', ['subcollection' => $id]);
    return $query->result_array();
  }

  public function search($query)
  {
    $this->db->like('title', $query);
    $this->db->or_like('author', $query);
    $result = $this->db->get('books');
    return $result->result_array();
  }

  public function getById($id)
  {
    $query = $this->db->get_where('books', ['id' => $id]);
    return $query->result_array();
  }

  public function add($data)
  {
    $this->db->set($data);
    $this->db->insert('books');
  }

  public function updateById($id, $data)
  {
    $this->db->where('id', $id);
    $this->db->set($data);
    $this->db->update('books');
  }

  public function removeById($id)
  {
    $this->db->where('id', $id);
    $this->db->delete('books');
  }
}

?>