<?php

class Subcollection_model extends CI_Model {
  public function __construct()
  {
    parent::__construct();
    $this->load->database();
  }

  public function getAll()
  {
    $query = $this->db->get('subcollections');
    return $query->result_array();
  }

  public function getById($id)
  {
    $query = $this->db->get_where('subcollections', ['id' => $id]);
    return $query->result_array();
  }

  public function add($data)
  {
    $this->db->set($data);
    $this->db->insert('subcollections');
  }

  public function updateById($id, $data)
  {
    $this->db->where('id', $id);
    $this->db->set($data);
    $this->db->update('subcollections');
  }

  public function removeById($id)
  {
    $this->db->where('id', $id);
    $this->db->delete('subcollections');
  }
}

?>