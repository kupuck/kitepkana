<?php

class Subscription_model extends CI_Model {
  public function __construct()
  {
    parent::__construct();
    $this->load->database();
  }

  public function getAll()
  {
    $query = $this->db->get('subscriptions');
    return $query->result_array();
  }

  public function getById($id)
  {
    $query = $this->db->get_where('subscriptions', ['id' => $id]);
    return $query->result_array();
  }

  public function updateById($id, $data)
  {
    $this->db->where('id', $id);
    $this->db->set($data);
    $this->db->update('subscriptions');
  }
}

?>