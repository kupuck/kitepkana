<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['default_controller'] = 'main';
$route['404_override'] = 'my404';
$route['translate_uri_dashes'] = FALSE;

$route['contacts'] = 'main/contacts';
$route['pricing'] = 'main/pricing';

$route['new'] = 'collections/new';
$route['collection/(:num)'] = 'collections/collection/$1';
$route['subcollection/(:num)'] = 'collections/subcollection/$1';
$route['book/(:num)'] = 'books/book/$1';
$route['search'] = 'books/search';

$route['signin'] = 'auth/signin';
$route['signup'] = 'auth/signup';
$route['signout'] = 'auth/signout';