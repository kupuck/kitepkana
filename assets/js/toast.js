let interval = setInterval(() => {
  if (typeof window.jQuery !== 'undefined') {
    $.toast = message => {
      if (!$('#toast').length) {
        $('body').append('<div id="toast"></div>');
      }
      $('#toast').html(message);
      $('#toast').addClass('show');
      setTimeout(() => $('#toast').removeClass('show'), 3000);
    };
    clearInterval(interval);
  }
}, 500);